import json
import yaml
import jsonschema
from yaml_error_report import error_report
from yaml_error_report.error_report import path_to_location
import os
import argparse
import colorama
colorama.init()


THIS_DIR = os.path.dirname(__file__)
SCHEMA_FILENAME = os.path.join(THIS_DIR, "schema.json")


def validate(filename: str):
    with open(SCHEMA_FILENAME, "r") as f:
        schema = json.load(f)

    with open(filename, "r") as f:
        instance = yaml.safe_load(f)
    try:
        jsonschema.validate(instance, schema)
        print("file in valid")
    except jsonschema.ValidationError as e:
        error_msg = e.message
        error_path = e.absolute_path
        location = path_to_location(filename, error_path)
        print(error_report("schema error", error_msg, location))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("input", help="input .yml or .json document filename")
    args = parser.parse_args()
    validate(args.input)


if __name__ == "__main__":
    main()
