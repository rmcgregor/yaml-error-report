# windows only: needed to make coloured output work
import colorama
colorama.init()

from yaml_error_report import path_to_location, error_report

# extract the text location of the offending node
loc = path_to_location("calc.yml", ["methods", 0, "request", 0, "type"])
# create the error report
error = error_report("YAML Validation Error", "Int36 is not a valid type", loc)
print(error)
