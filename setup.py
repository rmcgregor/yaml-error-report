from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='yaml-error-report',
    author='Robert Mcgregor',
    url="https://gitlab.com/rmcgregor/yaml-error-report",
    author_email='rmcgregor1990@gmail.com',
    description="Create console error reports for YAML (and JSON) documents",
    long_description=long_description,
    long_description_content_type="text/markdown",
    install_requires=['wrapt', "termcolor", "pyyaml"],
    packages=['yaml_error_report'],
    use_scm_version=True,
    python_requires=">=3.7",
    setup_requires=['setuptools_scm'],
    extras_require={"test": ["pytest"]},
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Text Processing",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Operating System :: OS Independent",
    ]
)
