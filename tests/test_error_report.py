from pathlib import Path
from yaml_error_report import path_to_location, error_report
from yaml_error_report.formatters import PlainFormatter

THIS_DIR = Path(__file__).parent
SAMPLE_FILE = THIS_DIR / "sample.yml"


def test_simple():
    loc = path_to_location(SAMPLE_FILE, ["methods", 0, "request", 0, "name"])
    error = error_report("Error", "bad name", loc, formatter=PlainFormatter())
    assert '      - name: "x"' in error
    assert "bad name" in error
