# yaml-error-report
[![pipeline status](https://gitlab.com/rmcgregor/yaml-error-report/badges/master/pipeline.svg)](https://gitlab.com/rmcgregor/yaml-error-report/commits/master)
[![coverage report](https://gitlab.com/rmcgregor/yaml-error-report/badges/master/coverage.svg)](https://rmcgregor.gitlab.io/yaml-error-report/coverage)


Generate rich console errors reports for YAML (and JSON, since its a YAML subset) documents.
This is aimed at console tools which want to present the user with useful errors messages if a YAML or JSON document fails some internal validation.

Uses PyYaml to extract location (line, column, position in stream) information for nodes in a YAML or JSON document.
This information can then be used to create user friendly error reports.

Can be combined with [jsonschema](https://json-schema.org/), to greatly improve error messages.
See [examples/with_jsonschema](https://gitlab.com/rmcgregor/yaml-error-report/-/tree/master/examples/with_jsonschema).


## Installation
```
pip install yaml-error-report

```
## Usage
```yaml
# calc.yml
name: calc

methods:
  - name: sum
    description: "Sums x and y"
    request:
      - name: "x"
        type: Int36
      - name: "y"
        type: Int32
    response: Int32
```

```python
# windows only: needed to make coloured output work
import colorama
colorama.init()

from yaml_error_report import path_to_location, error_report

# extract the text location of the offending node
loc = path_to_location("calc.yml", ["methods", 0, "request", 0, "type"])
# create the error report
error = error_report("YAML Validation Error", "Int36 is not a valid type", loc)
print(error)
```

*outputs (nicely coloured on terminals)*
```
YAML Validation Error
   --> calc.yml:8:14
   7|         - name: "x"
   8|           type: Int36
                      ^^^^^ Int36 is not a valid type
   9|         - name: "y"
  10|           type: Int32
```
